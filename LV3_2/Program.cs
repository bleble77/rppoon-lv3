﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_2
{
    class Program
    {
        static void Main(string[] args)
        {
            MatrixGenerator matrix = MatrixGenerator.GetInstance();
            double[][] createdMatrix;
            createdMatrix = matrix.CreateMatrix(5, 5);
            PrintMatrix.Print(createdMatrix);

        }
    }
}
