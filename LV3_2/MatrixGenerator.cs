﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV3_2
{
    class MatrixGenerator
    {
        private static MatrixGenerator instance;
        private Random random;
        private MatrixGenerator()
        {
            this.random = new Random();
        }

        public static MatrixGenerator GetInstance()
        {
            if (instance == null)
            {
                instance = new MatrixGenerator();
            }

            return instance;
        }

        public double[][] CreateMatrix(int rows, int columns)
        {
            double[][] matrix = new double[rows][];
            int i, j;
            for (i = 0; i < rows; i++)
            {
                matrix[i] = new double[columns];
            }

            for (i = 0; i < rows; i++)
            {
                for (j = 0; j < columns; j++)
                {
                    matrix[i][j] = random.NextDouble();
                }
            }

            return matrix;
        }
    }

    class PrintMatrix
    {
        public static void Print(double[][] matrix)
        {
            foreach (double[] row in matrix)
            {
                foreach (double real in row)
                {
                    Console.Write(real + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
